#!/bin/tcc -run -lpthread

#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <stdio.h>
#include <time.h>

#define	 	TRUE 		1
#define	 	FALSE 		0

unsigned int stack_len  = 0;
pthread_mutex_t mutex   = PTHREAD_MUTEX_INITIALIZER;
volatile char stop 	= FALSE;

typedef struct _node {
	char *msg;
	struct _node *nxt;
} node;

typedef struct {
	char *str;
	node **ptr;
} arg_t;

void delay (int millis) {
	millis *= 1000;
	clock_t timer = clock();
	while(clock() < timer + millis);
}

node *create_node (char *msg) {
	node *root = (node *) malloc(sizeof(node));
	root->msg  = (char *) malloc(sizeof(char) * strlen(msg));
	root->nxt  = NULL;
	strcpy(root->msg, msg);

	return root;
}

void destroy_node (node *root) {
	free(root->msg);
	free(root);
}

void push (node **root, char *msg) {
	printf("PUSH(\"%s\") \t -> %u\n", msg, stack_len);
	node *new_node = create_node(msg);
	new_node->nxt = *root;
	*root = new_node;
}

void pop (node **root) {
	if (*root == NULL)
		return;

	printf("POP(\"%s\") \t -> %u\n", (*root)->msg, stack_len);
	node *node_aux = *root;
	*root = (*root)->nxt;
	destroy_node(node_aux);
}

void *proc (void *ptr) {
	arg_t *inp = (arg_t *) ptr;
	pthread_mutex_lock(&mutex);
	push(inp->ptr, inp->str);
	stack_len++;
	pthread_mutex_unlock(&mutex);
	return NULL;
}

void interr (int sig) {
	if (sig == SIGINT)
		stop = TRUE;
}

int main (void) {
	node *root = create_node((char *) "***");
	pthread_t  thread_ptr[10];
	arg_t arg_ptr[10];
	char *msg;

	signal(SIGINT, interr);
	while (!stop) {
		for (int i = 0; i < 10; i++) {
			msg = (char *) malloc(sizeof(char) * 100);
			sprintf(msg, "Thread %d", i + 1);
			(arg_ptr + i)->str = msg;
			(arg_ptr + i)->ptr = &root;
			pthread_create(
				thread_ptr + i,
				NULL,
				proc,
				arg_ptr + i
			);
		}

		for (int i = 0; i < 10; i++) {
			pthread_join(thread_ptr[i], NULL);
			free((arg_ptr + i)->str);
		}

		delay(100);
	}

	printf("\n-------------------------------\n\n");

	while (root != NULL) {
		pop(&root);
		stack_len--;
	}

	return 0;
}
