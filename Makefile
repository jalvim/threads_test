#CC = gcc
CC = clang
CLFLAGS = -g -Wall
LL = -lpthread
OBJ = thread_test

$(OBJ): $(OBJ).c
	$(CC) $(CLFLAGS) $(LL) $(OBJ).c -o $(OBJ)
	chmod +x $(OBJ)

vec:	vec_threads.c
	$(CC) $(CFLAGS) $(LL) vec_threads.c -o vec_threads
	chmod +x vec_threads

clean:	$(OBJ) vec
	rm $(OBJ)
	rm vec_threads

max_th:
	cat /proc/sys/kernel/threads-max
