#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <pthread.h>
#include <time.h>

// Definição de macros gerais
#define 	TRUE 		1
#define 	FALSE 		0

// Definição de variáveis globais usadas
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
volatile char 	stop  = TRUE;
char 		flag  = TRUE;
int 		count = 0;
int 		lim   = 0;

void create_msgs (char *msgs[], int num_t) {
	/*
	 * 		RESUMO:
	 *
	 * Função criação de strings associadas
	 * a cada thread por meio da alocação na
	 * heap. Usar cleanup depois.
	 *
	 */

	for (int i = 0; i < num_t; i++) {
		msgs[i] = (char *) malloc(10 * sizeof(char));
		sprintf(msgs[i], "Thread %d", i + 1);
	}
}

void cleanup_msgs (char *msgs[], int num_t) {
	/*
	 * 		RESUMO:
	 *
	 * Função de liberação de memória de
	 * vetor de strings após seu uso pelo
	 * sistema.
	 *
	 */

	for (int i = 0; i < num_t; i++)
		free(msgs[i]);

	free(msgs);
}

void delay (int mili) {
	/*
	 * 		RESUMO:
	 *
	 * Função de cálculo de delay para milisegundos 
	 * a partir de entrada e número de ciclos
	 * do count_incressador.
	 *
	 */

	clock_t timer = clock();
	mili *= 1000;
	while (clock() < timer + mili);
}

void *count_incr (void *ptr) {
	/*
	 * 		RESUMO:
	 *
	 * Função de execução usada nas diferentes
	 * threads. Utiliza mutex para travar 
	 * atividades concorrentes.
	 *
	 */

	// Bloqueio de execução para acesso de memória global
	pthread_mutex_lock(&mutex);

	char *msg = (char *) ptr;
	printf("%s -> count: %d\n", msg, count);
	if ((count == lim) && (flag == TRUE)) 
		flag = FALSE;

	if ((count == 0) && (flag == FALSE))
		flag = TRUE;

	// Incremento ou decremento a depender de flag
	count += flag ? 1 : -1;

	// Desbloqueio de thread para execução concorrente
	pthread_mutex_unlock(&mutex);
	return NULL;
}

void handle_sigint (int sig) {
	/*
	 * 		RESUMO:
	 *
	 * Função de manejo de sinal de interrupção
	 * (acaba com loop principal de emulação).
	 * É ativada a partir da combinação <Ctrl-c>
	 *
	 */

	if (sig == SIGINT)
		stop = FALSE;
}

int main (int argc, char *argv[]) {
	// Manejo de número de argumentos
	if (argc != 3) {
		fprintf(
			stderr, 
			"ERRO: $ --> thread_test <LIMITE> <NÚMERO DE THREADS>\n"
		);
		return -1;
	}

	// Definição de variáveis globais e vetores dinâmicos associados a cada thread.
	lim 			= atoi(argv[1]);
	int  num_t 		= atoi(argv[2]);
	char **msgs 		= (char **) 	malloc(num_t * sizeof(char *));
	int  *ret 		= (int *) 	malloc(num_t * sizeof(int));
	pthread_t *threads 	= (pthread_t *)	malloc(num_t * sizeof(pthread_t));
	create_msgs(msgs, num_t);

	// Início de loop e manejo de sinal de interrupção (SIGINT)
	signal(SIGINT, handle_sigint);
	while (stop == TRUE) {
		// Loop de criação de threads usadas
		for (int i = 0; i < num_t; i++)
			ret[i] = pthread_create(
				threads + i,			// I-ésimo retorno da i-ésima thread
				NULL,				// Retorno para a função (defualt: NULL)
				count_incr,			// Função executada pela thread
				(void *) msgs[i]		// Usa a i-ésima mensagem como argumento
			);

		// Loop para junção das threads usadas
		for (int i = 0; i < num_t; i++)
			pthread_join(threads[i], NULL);		// Executa as threads de forma assíncrona

		printf("--------------------------\n");
		delay(250);					// Deixa o resultado mais fácil de se observar
	}

	// Ciclo de finalização de programa
	printf("\n");
	for (int i = 0; i < num_t; i++)
		printf("Retorno da thread %d: %d\n", i + 1, ret[i]);

	cleanup_msgs(msgs, num_t);
	free(threads);
	free(ret);
	printf("\nPrograma finalizado com sucesso!\n");
	exit(0);
}
