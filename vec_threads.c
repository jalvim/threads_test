#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

// Definição de tipo para a estrutura com argumentos de entrada p/ thread
typedef struct {
	int ii;				// Índice de linha
	int jj;				// Índice de coluna
	int len;			// Comprimento dos vetores
	int **address;			// Ponteiro para matriz calculada
	int **m1;			// Ponteiro para operando 1
	int **m2;			// Ponteiro para operando 2
} t_args;

void *inner_prod (void *ptr) {
	/*
	 * 		RESUMO:
	 *
	 * Função de cálculo para produto interno.
	 * Ela será usada pelas diversas threads
	 * em cada posição da matriz.
	 *
	 */

	t_args *args = (t_args *) ptr;
	int **ret = args->address;
	int **m1 = args->m1;
	int **m2 = args->m2;
	int i = args->ii, j = args->jj;
	ret[i][j] = 0;
	for (int idx = 0; idx < args->len; idx ++)
		ret[i][j] += (m1[i][idx] * m2[idx][j]);

	return NULL;
}

int **create_mat (char file_name[], int *l_num, int *c_num) {
	/*
	 * 		RESUMO:
	 *
	 * Função de leitura de arquivos de input
	 * e formalização das estruturas de dados
	 * usadas ao longo do programa. Os argumentos
	 * l_num e c_num são retornados via referência.
	 *
	 */

	int **mat;
	FILE *fp = fopen(file_name, "r");
	fscanf(fp, "%d", l_num);
	fscanf(fp, "%d", c_num);

	// Aloca espaço na Heap para matriz lida
	mat = (int **) malloc(*l_num * sizeof(int *));
	for (int i = 0; i < *l_num; i++)
		mat[i] = (int *) malloc(*c_num * sizeof(int));

	// Lê arquivo e armazena número lido na estrutura
	for (int i = 0; i < *l_num; i++)
		for (int j = 0; j < *c_num; j++)
			fscanf(fp, "%d", mat[i] + j);

	fclose(fp);
	return mat;
}

int **alloc_mat (int l_num, int c_num) {
	/*
	 * 		RESUMO:
	 *
	 * Função de alocação de espaço para
	 * a matriz de resultados na heap.
	 */

	int **res = (int **) malloc(l_num * sizeof(int *));
	for (int i = 0; i < l_num; i++)
		res[i] = (int *) malloc(c_num * sizeof(int));

	return res;
}

void destroy_mat (int **mat, int l_num) {
	/*
	 * 		RESUMO:
	 *
	 * Função de desalocação de espaço para
	 * a matriz de resultados na heap.
	 */

	for (int i = 0; i < l_num; i++)
		free(mat[i]);

	free(mat);
}

void print_init (void) {
	/*
	 * 		RESUMO:
	 *
	 * Função de impressão de mensagem 
	 * inicial do programa.
	 *  
	 */

	printf("Programa de multiplicação multithreded de matrizes.\n");
	printf("O programa efetuará a multiplicação M1 x M2\n");
	printf("Com as matrizes M1 em Inputs/mat_1 e M2 em Inputs/mat_2\n\n");
}

void print_res (int **res, int l_num, int c_num) {
	/*
	 * 		RESUMO:
	 *
	 * Função de impressão de reultado 
	 * obtido pela multiplicação matricial.
	 *  
	 */

	printf("Resultado da multiplicação:\n\n");
	for (int i = 0; i < l_num; i++) {
		for (int j = 0; j < c_num; j++)
			printf("%d ", res[i][j]);

		printf("\n");
	}
}

int main (void) {
	print_init();
	int l_num1, c_num1;
	int l_num2, c_num2;
	int **mat_1 = create_mat("./Inputs/mat_1", &l_num1, &c_num1);
	int **mat_2 = create_mat("./Inputs/mat_2", &l_num2, &c_num2);

	// Manejo de excessão em caso de incompatibilidade das matrizes
	if (c_num1 != l_num2) {
		fprintf(stderr, "ERRO número de colunas de m1 deve ser igual ao número de linhas de m2\n");
		destroy_mat(mat_1, l_num1); 			// Libera memória já alocada
		destroy_mat(mat_2, l_num2); 			// Libera memória já alocada
		return -1;					// Encerra programa com excessão
	}

	// Declaração de variáceis associadas à matriz de resultados
	int len   = c_num1;
	int **res = alloc_mat(l_num1, c_num2);
	int cnt   = 0;

	// Alocação de vetor de threads e argumentos usados pelas threads
	pthread_t *t_vec = (pthread_t *) malloc(l_num1 * c_num2 * sizeof(pthread_t));
	t_args 	  *args  = (t_args *)    malloc(l_num1 * c_num2 * sizeof(t_args));

	// Loop de criação de threads para cada elemento da nova matriz
	for (int i = 0; i < l_num1; i++)
		for (int j = 0; j < c_num2; j++) {
			(args + cnt)->address 	= res;
			(args + cnt)->len 	= len;
			(args + cnt)->ii 	= i;
			(args + cnt)->jj 	= j;
			(args + cnt)->m1 	= mat_1;
			(args + cnt)->m2 	= mat_2;
			pthread_create(
				t_vec + cnt,
				NULL,
				inner_prod,
				args + cnt
			);
			cnt++;
		}

	// Executa as threads de maneira assíncrona
	for (int i = 0; i < l_num1 * c_num2; i++)
		pthread_join(t_vec[i], NULL);

	// Impressão da matriz calculada
	print_res(res, l_num1, c_num2);

	// Liberação da memória após finalização do programa
	free(t_vec);
	free(args);
	destroy_mat(mat_1, l_num1);
	destroy_mat(mat_2, l_num2);
	destroy_mat(res, l_num1);
	return 0;
}
